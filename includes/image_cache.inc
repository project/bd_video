<?php

// Copyright 2007-2009 Jonathan Brown


function _bd_video_render_image($video, $image, $width, $height) {

  $file_id = db_result(db_query("
    SELECT file_id
    FROM {bd_video_image_cache}
    WHERE video_id = %d AND 
      image = '%s' AND
      width = %d AND
      height = %d
  ",
    $video['video_id'], $image, $width, $height
  ));
  
  if($file_id)
    return storage_api_serve_url(storage_api_file_load($file_id));


  $in_file = storage_api_get_local_filepath($video[$image . '_image_file'], $keep_me);

  $info = image_get_info($in_file);
  
  if(!$info) {
    if(!$keep_me)
      @unlink($in_file);
    return;
  }
  
  if($width / $height > $info['width'] / $info['height']) {
  
    $scale_height = $height;
    $scale_width = (int)round(($info['width'] * $height) / $info['height']);
    $left = (int)round(($width - $scale_width) / 2);
  }
  else {
    $scale_width = $width;
    $scale_height = (int)round(($info['height'] * $width) / $info['width']);
    $top = (int)round(($height - $scale_height) / 2);
  }
  
  
  include_once 'includes/image.gd.inc';
  $src = image_gd_open($in_file, $info['extension']);
  
  if(!$keep_me)
    @unlink($in_file);

  if(!$src)
    return;
  
  $dest = imagecreatetruecolor($width, $height);
  
  if(!$dest) {
    imagedestroy($src);
    return;
  }
  
  $success = imagecopyresampled($dest, $src, $left, $top, 0, 0, 
    $scale_width, $scale_height, $info['width'], $info['height']);

  imagedestroy($src);
  
  if(!$success) {
    imagedestroy($dest);
    return;
  }
  
  $out_file = tempnam(file_directory_temp(), '');
  imageinterlace($dest, TRUE);
  $success = image_gd_close($dest, $out_file, 'jpeg');
  imagedestroy($dest);
  
  if(!$success) {
    @unlink($out_file);
    return;
  }
  
  $selector_ids = _bd_video_get_selector_ids($video['field']);

  $options = array(
    'module' => 'bd_video',
    'type' => 'scaled preview image'
  );

  $file_id = storage_api_add_file_from_filepath($out_file, $selector_ids['cache'], $options);
  @unlink($out_file);
  
  db_query("
    INSERT INTO {bd_video_image_cache}
    SET video_id = %d,
      image = '%s',
      width = %d,
      height = %d,
      file_id = %d
  ",
    $video['video_id'], $image, $width, $height, $file_id
  );
  
  return storage_api_serve_url(storage_api_file_load($file_id));
}


function _bd_video_purge_image_cache($video_id = NULL) {

  if($video_id)
    $where = 'WHERE video_id = %d';

  $result = db_query("
    SELECT *
    FROM {bd_video_image_cache}
    $where
  ",
    $video_id
  );
  
  while($file = db_fetch_array($result)) {
    storage_api_remove_file($file['file_id']);
  }
  
  db_query("
    DELETE FROM {bd_video_image_cache}
    $where
  ",
    $video_id
  );
}

