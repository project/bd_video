<?php

// Copyright 2007-2009 Jonathan Brown


define("BD_VIDEO_DEFAULT_ALLOW_REPLACE", 0);
define("BD_VIDEO_DEFAULT_ALLOW_EMBED", 0);


/*
      FIELD
*/


function bd_video_field_info() {

  return array(
    'bd_video' => array(
      'label' => 'Blue Droplet Video',
      'description' => 'Video'
    )
  );
}


function _bd_video_get_params_id($field) {
  $params_ids = variable_get('bd_video_field_params_ids', array());
  return $params_ids[$field['field_name']];
}


function _bd_video_field_load_params($field) {
  $params_ids = variable_get('bd_video_field_params_ids', array());

  if(!$params_ids[$field['field_name']]) {
    $params_ids[$field['field_name']] = _bd_video_transcoding_params_insert();
    variable_set('bd_video_field_params_ids', $params_ids);
  }
  
  return _bd_video_transcoding_params_load($params_ids[$field['field_name']]);
}


function _bd_video_field_retranscode_all($field) {

  $table = 'content_type_' . $field['type_name'];
  $video_id_field = $field['field_name'] . '_video_id';

  $result = db_query("
    SELECT {bd_video}.video_id
    FROM {{$table}}
    INNER JOIN {bd_video}
      ON {{$table}}.$video_id_field = {bd_video}.video_id
    WHERE {bd_video}.source_file_id IS NOT NULL
  ");

  while($video = db_fetch_array($result)) {
    _bd_video_retranscode_video($video['video_id']);
  }

  $msg = t('Retranscoding all field %label videos.', array('%label' => $field['label']));
  drupal_set_message($msg);
  watchdog('bd_video', $msg, NULL);
}


function _bd_video_field_store_params($field) {
  $params_id = _bd_video_get_params_id($field);
  _bd_video_transcoding_params_update($params_id, $field);
  
  if($field['retranscode_all']) {
    _bd_video_field_retranscode_all($field);
  }
}


function _bd_video_get_selector_ids($field_name) {

  $selector_ids = variable_get('bd_video_field_selector_ids', array());

  foreach(array('source', 'transcoded', 'cache') as $selector) {

    if($selector_ids[$field_name][$selector]) {
    
      // this will fix a situation where storage_api was uninstalled, but bd_video was not
    
      $new = !db_result(db_query("
        SELECT COUNT(*)
        FROM {storage_selector}
        WHERE selector_id = %d
      ",
        $selector_ids[$field_name][$selector]
      ));
    }
    else {    
      $new = TRUE;
    }
    
    if($new) {
      $selector_ids[$field_name][$selector] = storage_api_new_selector_id();
      $update = TRUE;
    }
  }
  
  if($update)
    variable_set('bd_video_field_selector_ids', $selector_ids);
  
  return $selector_ids[$field_name];
}


function _bd_video_selectors_fieldset($field_name) {

  $selector_ids = _bd_video_get_selector_ids($field_name);
  
  $fieldset = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Storage classes'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  
  $fieldset['source'] = storage_api_selector_item($selector_ids['source'], t('Source class'),
    t('Source video and custom preview images.'));
    
  $fieldset['transcoded'] = storage_api_selector_item($selector_ids['transcoded'], t('Transcoded class'),
    t('Transcoded video and captured preview images.'));
    
  $fieldset['cache'] = storage_api_selector_item($selector_ids['cache'], t('Cache class'),
    t('Cached scaled images.'));
      
  return $fieldset;
}


function bd_video_field_settings($op, $field) {

  switch ($op) {

    case 'form':
    
      $description = t("Allow the video the be embedded on other websites.") . '<br />';
      $description .= t("Users with <em>administer video</em> permission can always do this.");

      $form['allow_embed'] = array(
        '#type' => 'checkbox',
        '#title' => 'Allow embedding',
        '#default_value' => isset($field['allow_embed']) ? $field['allow_embed'] : BD_VIDEO_DEFAULT_ALLOW_EMBED,
        '#description' => $description
      );
      
      $form['preview_image_settings'] = _bd_video_preview_image_settings_fieldset($field);
      $form['player_settings'] = _bd_video_player_settings_fieldset($field);
      
      $params = _bd_video_field_load_params($field);
      $form['transcoding_params'] = _bd_video_transcoding_params_fieldset($params);
      
      $form['selectors'] = _bd_video_selectors_fieldset($field['field_name']);

      return $form;


    case 'validate':
    
      _bd_video_preview_image_settings_validate($field);
      _bd_video_player_settings_validate($field);
      _bd_video_transcoding_params_validate($field);
      
      if(!form_get_errors()) {    // $form data is not passed to 'save' op below
        _bd_video_field_store_params($field);
        storage_api_selector_submit($field['selectors']['source']);
        storage_api_selector_submit($field['selectors']['transcoded']);
        storage_api_selector_submit($field['selectors']['cache']);
      }
      return;


    case 'save':
    
      _bd_video_purge_image_cache();
      $preview_image_fields = _bd_video_player_settings_fields();
      $player_fields = _bd_video_preview_image_settings_fields();
      return array_merge(array('allow_embed'), $preview_image_fields, $player_fields);


    case 'database columns':

      return array(
        'video_id' => array('type' => 'int', 'unsigned' => TRUE, 'not null' => FALSE)
      );

      
    case 'views data':
    
      return _bd_video_views_data($field);
  }
}


function bd_video_field($op, &$node, $field, &$items, $teaser, $page) {

  if($items[0]['custom_image_file_id'])
    $items[0]['preview_image']['preview_image'] = 'custom';

  switch ($op) {
  
    case 'validate':

      if(!($items[0]['source_file']['uploaded']['file'] && 
          !$items[0]['source_file']['uploaded']['ignore']) &&
        !$items[0]['source_file']['acquire_url'] &&
        $items[0]['source_file']['filepath'])
      {
        if(!is_file($items[0]['source_file']['filepath'])) {
          form_set_error($field['field_name'] . '][0][source_file][filepath', t('File not found.'));
        }
      }

      if($field['required'] && !$items[0]['video_id'] &&
        !($items[0]['source_file']['uploaded']['file'] && 
          !$items[0]['source_file']['uploaded']['ignore']) &&
        !$items[0]['source_file']['acquire_url'] &&
        !$items[0]['source_file']['filepath'])
      {
        form_set_error($field['field_name'], t('You need add a video.'));
      }

      if(!($items[0]['preview_image']['source_file']['uploaded']['file'] && 
          !$items[0]['preview_image']['source_file']['uploaded']['ignore']) &&
        !$items[0]['preview_image']['source_file']['acquire_url'] &&
        $items[0]['preview_image']['source_file']['filepath'])
      {
        if(!is_file($items[0]['preview_image']['source_file']['filepath'])) {
          form_set_error($field['field_name'] . '][0][preview_image][source_file][filepath', t('File not found.'));
        }
      }

      break;
      
      
    case 'presave':
    
      $selector_ids = _bd_video_get_selector_ids($field['field_name']);

      if($items[0]['video_id'])
        $video = _bd_video_load($items[0]['video_id']);

      $options = array(
        'module' => 'bd_video',
        'type' => 'source video',
        'force_download' => TRUE
      );
      
      $file = $items[0]['source_file']['uploaded']['file'];

      if($file && !$items[0]['source_file']['uploaded']['ignore']) {
        $options['filename'] = $file->filename;
          
        $items[0]['source_file_id'] = 
          storage_api_add_file_from_filepath($file->filepath, $selector_ids['source'], $options);
      }
      else if($items[0]['source_file']['acquire_url']) {

        $items[0]['source_file_id'] = 
          storage_api_add_file_from_url($items[0]['source_file']['acquire_url'], $selector_ids['source'], $options);
      }
      else if($items[0]['source_file']['filepath']) {
        $options['filename'] = basename($items[0]['source_file']['filepath']);

        $items[0]['source_file_id'] = 
          storage_api_add_file_from_filepath($items[0]['source_file']['filepath'], $selector_ids['source'], $options);
      }

      if($video || $items[0]['source_file_id']) {
      
        $options = array(
          'module' => 'bd_video',
          'type' => 'custom preview image',
          'force_download' => TRUE
        );

        $file = $items[0]['preview_image']['source_file']['uploaded']['file'];

        if($file && !$items[0]['preview_image']['source_file']['uploaded']['ignore']) {
          $options['filename'] = $file->filename;
            
          $items[0]['custom_image_file_id'] = 
            storage_api_add_file_from_filepath($file->filepath, $selector_ids['source'], $options);
        }
        else if($items[0]['preview_image']['source_file']['acquire_url']) {

          $items[0]['custom_image_file_id'] = 
            storage_api_add_file_from_url($items[0]['preview_image']['source_file']['acquire_url'], $selector_ids['source'], $options);
        }
        else if($items[0]['preview_image']['source_file']['filepath']) {
          $options['filename'] = basename($items[0]['source_file']['filepath']);

          $items[0]['custom_image_file_id'] = 
            storage_api_add_file_from_filepath($items[0]['preview_image']['source_file']['filepath'], $selector_ids['source'], $options);
        }
      }

      break;
      

    case 'update':
      
      if($items[0]['video_id']) {
        $video = _bd_video_load($items[0]['video_id']);

        if($items[0]['source_file_id']) {
          _bd_video_update_source_file($video, $items[0]['source_file_id']);
          drupal_set_message('The new video will be transcoded.');
        }
        else if($items[0]['source_file']['delete']) {
          _bd_video_delete($video);
          unset($items[0]['video_id']);
          break;
        }
        else if($items[0]['retranscode']) {
          _bd_video_retranscode_video($items[0]['video_id']);
          drupal_set_message('The video will be retranscoded.');
        }
        
        if($items[0]['custom_image_file_id']) {
          _bd_video_update_custom_image($video, $items[0]['custom_image_file_id']);
        }
        else if($items[0]['preview_image']['source_file']['delete']) {
        
          _bd_video_update_custom_image($video, 0);
          
          if($items[0]['preview_image']['preview_image'] == 'custom')
            $items[0]['preview_image']['preview_image'] = $field['preview_image_image'];
        }

        if($items[0]['preview_image']['preview_image'] && 
          ($items[0]['preview_image']['preview_image'] != $video['preview_image'])) {
          _bd_video_set_preview_image($items[0]['video_id'], $items[0]['preview_image']['preview_image']);
        }
        
        break;
      }
      
      // follow through

    case 'insert':
    
      if($items[0]['source_file_id']) {

        $items[0]['video_id'] = 
          _bd_video_create_video($items[0]['source_file_id'], _bd_video_get_params_id($field), 
            $items[0]['preview_image']['preview_image'], $items[0]['custom_image_file_id'],
            $field['field_name'], $node->nid, $items[0]['flv_file_id'], $items[0]['duration']);
      }
            
      break;
  
      
    case 'delete':
    
      if($items[0]['video_id'])
        _bd_video_delete($items[0]['video_id']);
        
      break;
  }
}



/*
      WIDGET
*/



function bd_video_widget_info() {

  return array(
    'bd_video' => array(
      'label' => t('Upload / Acquire from URL / Filepath'),
      'field types' => array('bd_video'),
    )
  );
}


function bd_video_widget_settings($op, $widget) {

  switch ($op) {

    case 'form':
    
      $description = t("Allow the video the be replaced / deleted after the node has been created.") . "<br />\n";
      $description .= t("Users with <em>administer video</em> permission can always do this.");

      $form['allow_replace'] = array(
        '#type' => 'checkbox',
        '#title' => 'Allow replacement',
        '#default_value' => isset($widget['allow_replace']) ? $widget['allow_replace'] : BD_VIDEO_DEFAULT_ALLOW_REPLACE,
        '#description' => $description
      );
      
      return $form;

    case 'save':
      return array('allow_replace');

    case 'callbacks':
      return array(
        'view' => CONTENT_CALLBACK_CUSTOM,
      );
  }
}


function bd_video_content_is_empty($item, $field) {

  if(!$item['video_id'] && !$item['source_file_id'])
    return TRUE;
  else
    return FALSE;
}


function _bd_video_widget_upload_form_item($text, $field, $state, &$storage, $existing, $required = FALSE) {

  if($existing) {
  
    if(!$required) {
      $item['delete'] = array(
        '#type' => 'checkbox',
        '#title' => t('Delete this') . ' ' . t($text),
      );
    }
    
    $item[] = array(
      '#value' => '<div style="clear: both;"></div>'
    );
  }
  
  $file = file_save_upload($field['field_name']);

  if($file) {
    $storage[$field['field_name']] = $file;
  }
  else {
    
    if(!$state['uploaded']['ignore'])
      $file = $storage[$field['field_name']];
  }

  if($file) {
    $info = 'Filename: ' . $file->filename . '<br />';
    $info .= 'Size: ' . storage_format_size($file->filesize);
    $item['uploaded'] = array(
      '#type' => 'fieldset',
      '#title' => t('Uploaded') . ' ' . t($text)
    );
    $item['uploaded'][] = array(
      '#value' => $info
    );
    $item['uploaded']['ignore'] = array(
      '#type' => 'checkbox',
      '#title' => t('Ignore')
    );
    $item['uploaded']['file'] = array(
      '#type' => 'value',
      '#value' => $file
    );
  }

  $item[$field['field_name']] = array(
    '#type' => 'file', 
    '#name' => 'files[' . $field['field_name'] . ']',
    '#description' => 'Max size: ' . storage_format_size(file_upload_max_size())
  );

  if(!$existing && !$file)
    $item[$field['field_name']]['#title'] = t('Upload') . ' ' . t($text);
  else
    $item[$field['field_name']]['#title'] = t('Upload new') . ' ' . t($text);
  
  $item['acquire_url'] = array(
    '#type' => 'textfield',
    '#title' => 'Acquire from URL',
    '#default_value' => $state['acquire_url'],    // for preview
  );

  if(user_access('administer video')) {
    $item['filepath'] = array(
      '#type' => 'textfield',
      '#title' => 'Filepath',
      '#default_value' => $state['filepath'],    // for preview
      '#autocomplete_path' => BD_VIDEO_FILEPATH_AUTOCOMPLETE_PATH
    );
  }
  
  return $item;
}


function _bd_video_widget_preview_image($field, $items, &$storage) {

  $item = array(
    '#type' => 'fieldset',
    '#title' => t('Preview image')
  );
  
  if($items[0]['video_id']) {
    $video = _bd_video_load($items[0]['video_id']);
  }
  
  if($video['start_image_file_id'])
    $options['start'] = _bd_video_render_preview_image($video, 80, 60, NULL, 'start') . ' - start of video';
  else
    $options['start'] = 'Start of video';
  
  if($video['third_image_file_id'])
    $options['third'] = _bd_video_render_preview_image($video, 80, 60, NULL, 'third') . ' - @ 1/3 of video duration';
  else
    $options['third'] = '@ 1/3 of video duration';
    
  $existing = $video['custom_image_file_id'] != 0;
  
  if($existing)
    $options['custom'] = _bd_video_render_preview_image($video, 80, 60, NULL, 'custom') . ' - custom image';

  $item['preview_image'] = array(
    '#type' => 'radios',
    '#default_value' => isset($video['preview_image']) ? $video['preview_image'] : 
      (isset($field['preview_image_image']) ? $field['preview_image_image'] : BD_VIDEO_DEFAULT_PREVIEW_IMAGE_IMAGE),
    '#options' => $options
  );

  $field['field_name'] .= '_image';
  $item['source_file'] = _bd_video_widget_upload_form_item('custom image', $field, $items[0]['preview_image']['source_file'], $storage, $existing);

  return $item;
}


function bd_video_widget(&$form, &$form_state, $field, $items, $delta = 0) {

  $fieldset = array(
    '#type' => 'fieldset',
    '#title' => $field['widget']['label'],
    '#description' => content_filter_xss($field['widget']['description'])
  );
  
  $existing = $items[0]['video_id'] != 0;
  
  if($existing) {
  
    $fieldset['video_id'] = array(
      '#type' => 'value',
      '#value' => $items[0]['video_id']
    );

    $video = _bd_video_load($items[0]['video_id']);
    $filepath = $video['source_file']['filepath'];
    
    $info = _bd_video_render_preview_image($video, 240, 180, array('style="float: left; margin-right: 10px;"'));
  
    $info .= 'Filename: ' . $video['source_file']['filename'] . '<br />';

    if($video['source_file']['size']) {
      $info .= 'Size: ' . storage_format_size($video['source_file']['size']) . '<br />';
    }
    
    if($video['duration'])
      $info .= 'Duration: ' . format_interval($video['duration']) . '<br />';
    
    $fieldset[] = array(
      '#value' => '<p>' . $info . '</p>'
    );

    if(user_access('administer video') && $video['source_file_id']) {
      
      $fieldset['retranscode'] = array(
        '#type' => 'checkbox', 
        '#title' => t('Retranscode this video')
      );
    }
  }
  
  if(!$existing || $field['widget']['allow_replace'] || user_access('administer video')) {
  
    $fieldset['source_file'] = 
      _bd_video_widget_upload_form_item('video', $field, $items[0]['source_file'], $form_state['storage'], $existing, $field['required']);
  }
  else {
    $fieldset[] = array(
      '#value' => '<div style="clear: both;"></div>'
    );
  }      

  if(isset($field['preview_image_user_selectable']) ? $field['preview_image_user_selectable'] :
    BD_VIDEO_DEFAULT_PREVIEW_IMAGE_USER_SELECTABLE) // || user_access('administer video'))
  {
    $fieldset['preview_image'] = _bd_video_widget_preview_image($field, $items, $form_state['storage']);
  }

  $form['#attributes']['enctype'] = "multipart/form-data";
  $form['#submit'][] = 'bd_video_field_submit';
  
  return $fieldset;
}


function bd_video_field_submit($form, &$form_state) {

  if($form_state['clicked_button']['#value'] == 'Save')
    unset($form_state['storage']);    // make sure we redirect: http://drupal.org/node/144132#form-state
}



/*
      FORMATTERS
*/



function bd_video_field_formatter_info() {

  return array(
    'default' => array(
      'label' => 'Inline player',
      'field types' => array('bd_video'),
    ),
    'preview_image_link' => array(
      'label' => 'Preview image - link to node',
      'field types' => array('bd_video'),
    )
  );
}


function theme_bd_video_formatter_default($element) {

  $video = _bd_video_load($element['#item']['video_id']);
  _bd_video_set_render_settings(content_fields($element['#field_name'], $element['#type_name']));
  
  return _bd_video_render_video($video, 'default');
}


function theme_bd_video_formatter_preview_image_link($element) {

  $video = _bd_video_load($element['#item']['video_id']);
  _bd_video_set_render_settings(content_fields($element['#field_name'], $element['#type_name']));
  
  return _bd_video_render_video($video, 'preview-image-link', $element['#node']->nid);
}


function _bd_video_get_first_video_field($node) {
  $content_type_info = _content_type_info();
  
  foreach((array)$node as $field => $value) {
    if($content_type_info['fields'][$field]['type'] == 'bd_video') {
      return $field;
    }
  }
}


function _bd_video_get_video_fields($node) {
  $content_type_info = _content_type_info();
  $fields = array();
  
  foreach((array)$node as $field => $value) {
    if($content_type_info['fields'][$field]['type'] == 'bd_video') {
      $fields[] = $field;
    }
  }
  
  return $fields;
}


function _bd_video_get_first_video($node) {

  $field = _bd_video_get_first_video_field($node);
  
  if($field)
     return _bd_video_load($node->{$field}[0]['video_id']);
}


function bd_video_nodeapi($node, $op, $teaser, $page) {

  if($op != 'rss item')
    return;
    
  $video = _bd_video_get_first_video($node);

  if($video['source_file']) {

    $rss_items[] = array(
      'key' => 'enclosure',
      'attributes' => array(
        'url' => storage_api_serve_url($video['source_file'], TRUE),
        'length' => $video['source_file']['size'],
        'type' => $video['source_file']['mimetype']
      )
    );
    
    return $rss_items;
  }
}

