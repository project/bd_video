<?php

// Copyright 2007-2009 Jonathan Brown


function bd_video_feedapi_mapper($op, $node, $field_name, $feed_element = array(), $sub_field = '') {

  $field = content_fields($field_name);
  if($field['type'] != 'bd_video') {
    return;
  }

  switch($op) {
  
    case 'describe':
      $info  = 'Map this field to options->enclosures->video to pull in video content.<br />';
      $info .= 'Note that Common syndication parser does not parse enclosures - use SimplePie parser instead.';
      return t($info);
      
    case 'list':
      return TRUE;
      
    case 'map';
    
      if(!is_array($feed_element))
        return $node;
    
      $format = array_shift($feed_element);
      $video = $format[0];
    
      $field = $node->$field_name;

      $selector_ids = _bd_video_get_selector_ids($field_name);
      
      $options = array(
        'module' => 'bd_video',
        'type' => 'source video',
        'force_download' => TRUE
      );
      
      $field[0]['source_file_id'] = storage_api_add_file_from_url($video['link'], $selector_ids['source'], $options);
      
      $options = array(
        'module' => 'bd_video',
        'type' => 'custom preview image'
      );
      
      $field[0]['custom_image_file_id'] = storage_api_add_file_from_url($video['thumbnails'][0], 
        $selector_ids['source'], $options);
        
      $node->$field_name = $field;
      return $node;
  }
}
