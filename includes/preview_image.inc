<?php

// Copyright 2007-2008 Jonathan Brown

define("BD_VIDEO_DEFAULT_PREVIEW_IMAGE_WIDTH", 320);
define("BD_VIDEO_DEFAULT_PREVIEW_IMAGE_HEIGHT", 240);
define("BD_VIDEO_DEFAULT_PREVIEW_IMAGE_IMAGE", 'third');
define("BD_VIDEO_DEFAULT_PREVIEW_IMAGE_USER_SELECTABLE", 1);


function _bd_video_preview_image_settings_fields() {

  return array(
    'preview_image_width',
    'preview_image_height',
    'preview_image_image',
    'preview_image_user_selectable'
  );
}


function _bd_video_preview_image_settings_fieldset($settings = NULL) {

  $fieldset = array(
    '#type' => 'fieldset',
    '#title' => t('Preview image'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  
  $fieldset['preview_image_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Default width'),
    '#default_value' => $settings['preview_image_width'] ? $settings['preview_image_width'] : BD_VIDEO_DEFAULT_PREVIEW_IMAGE_WIDTH,
    '#description' => t('pixels'),
    '#size' => 6,
    '#maxlength' => 4,
    '#required' => TRUE
  );

  $fieldset['preview_image_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Default height'),
    '#default_value' => $settings['preview_image_height'] ? $settings['preview_image_height'] : BD_VIDEO_DEFAULT_PREVIEW_IMAGE_HEIGHT,
    '#description' => t('pixels'),
    '#size' => 6,
    '#maxlength' => 4,
    '#required' => TRUE
  );
  
  $fieldset['preview_image_image'] = array(
    '#type' => 'radios',
    '#title' => t('Image'),
    '#default_value' => $settings['preview_image_image'] ? $settings['preview_image_image'] : BD_VIDEO_DEFAULT_PREVIEW_IMAGE_IMAGE,
    '#options' => array(
      'start' => 'Start of video',
      'third' => '@ 1/3 of video duration'
    )
  );

  $fieldset['preview_image_user_selectable'] = array(
    '#type' => 'checkbox',
    '#title' => 'User selectable',
//    '#default_value' => $settings['preview_image_user_selectable'] ? $settings['preview_image_user_selectable'] : BD_VIDEO_DEFAULT_PREVIEW_IMAGE_USER_SELECTABLE
    '#default_value' => $settings['preview_image_user_selectable']
  );
    
  return $fieldset;
}


function _bd_video_preview_image_settings_validate($form_values) {
  ;
}


function _bd_video_preview_image_settings_submit(&$form_values) {
  _bd_video_purge_image_cache();
}


function _bd_video_set_preview_image($video_id, $preview_image) {
  
  db_query("
    UPDATE {bd_video}
    SET preview_image = '%s'
    WHERE video_id = %d
  ",
    $preview_image,
    $video_id
  );
}


function _bd_video_get_preview_image($video) {
  if(_bd_video_get_render_setting('preview_image_user_selectable', BD_VIDEO_DEFAULT_PREVIEW_IMAGE_USER_SELECTABLE)) {
    return $video['preview_image'];
  }
  else {
    return _bd_video_get_render_setting('preview_image_image', BD_VIDEO_DEFAULT_PREVIEW_IMAGE_IMAGE);
  }
}


function _bd_video_render_preview_image($video, $width = NULL, $height = NULL, $extra = NULL, $image = NULL) {

  if(!$width)
    $width = _bd_video_get_render_setting('preview_image_width', BD_VIDEO_DEFAULT_PREVIEW_IMAGE_WIDTH);
    
  if(!$height)
    $height = _bd_video_get_render_setting('preview_image_height', BD_VIDEO_DEFAULT_PREVIEW_IMAGE_HEIGHT);
  
  if(!$image)
    $image = _bd_video_get_preview_image($video);
  
  $url = _bd_video_render_image($video, $image, $width, $height);

  if(!$url)
    return;

  $parts[] = '<img';
  $parts[] = 'src="' . $url . '"';
  $parts[] = 'width="' . $width . '"';
  $parts[] = 'height="' . $height . '"';
  $parts[] = 'alt="Video preview image"';
  
  if(is_array($extra))
    $parts = array_merge($parts, $extra);

  $parts[] = '/>';

  return implode(' ', $parts); 
}


function _bd_video_render_preview_image_link($video, $width = NULL, $height = NULL) {
  $thumb = _bd_video_render_preview_image($video, $width, $height);
  return l($thumb, 'node/' . $video['nid'], array('html' => TRUE));
}

