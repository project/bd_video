<?php

// Copyright 2007-2009 Jonathan Brown


define("BD_VIDEO_DEFAULT_PLAYER_WIDTH", 640);
define("BD_VIDEO_DEFAULT_PLAYER_HEIGHT", 360);
define("BD_VIDEO_DEFAULT_ICONS", TRUE);
define("BD_VIDEO_DEFAULT_CONTROLBAR", TRUE);
define("BD_VIDEO_DEFAULT_SHOWDOWNLOAD", FALSE);
define("BD_VIDEO_DEFAULT_AUTOSTART", TRUE);
define("BD_VIDEO_DEFAULT_REPEAT", FALSE);
define("BD_VIDEO_DEFAULT_MUTE", FALSE);
define("BD_VIDEO_DEFAULT_VOLUME", 80);
define("BD_VIDEO_DEFAULT_BUFFERLENGTH", 5);
define("BD_VIDEO_DEFAULT_BACKCOLOR", 'FFFFFF');
define("BD_VIDEO_DEFAULT_FRONTCOLOR", '000000');
define("BD_VIDEO_DEFAULT_LIGHTCOLOR", '000000');
define("BD_VIDEO_DEFAULT_SCREENCOLOR", '000000');

define("BD_VIDEO_MAX_VOLUME", 100);
define("BD_VIDEO_MAX_BUFFER_LENGTH", 60);


function _bd_video_player_settings_fields() {

  return array(
    'player_settings_saved',
    'player_width',
    'player_height',
    'icons',
    'controlbar',
    'showdownload',
    'autostart',
    'repeat',
    'mute',
    'volume',
    'bufferlength',
    'backcolor',
    'frontcolor',
    'lightcolor',
    'screencolor'
  );
}


function _bd_video_player_settings_fieldset($settings = NULL) {

  $fieldset = array(
    '#type' => 'fieldset',
    '#title' => t('Video player'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );

  $fieldset['player_settings_saved'] = array(
    '#type' => 'value',
    '#value' => TRUE
  );

  $fieldset['player_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Video width'),
    '#default_value' => $settings['player_settings_saved'] ? $settings['player_width'] : BD_VIDEO_DEFAULT_PLAYER_WIDTH,
    '#description' => t('pixels'),
    '#size' => 6,
    '#maxlength' => 4,
    '#required' => TRUE
  );

  $fieldset['player_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Video height'),
    '#default_value' => $settings['player_settings_saved'] ? 
      $settings['player_height'] : BD_VIDEO_DEFAULT_PLAYER_HEIGHT,
    '#description' => t('pixels'),
    '#size' => 6,
    '#maxlength' => 4,
    '#required' => TRUE
  );

  $fieldset['icons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display icons'),
    '#default_value' => $settings['player_settings_saved'] ? $settings['icons'] : BD_VIDEO_DEFAULT_ICONS,
    '#description' => t('Show or hide the play and activity icons in the middle of the video.'),
  );

  $fieldset['controlbar'] = array(
    '#type' => 'select',
    '#title' => t('Controlbar'),
    '#options' => array(
      'over' => t('Overlay - fades away'),
      'bottom' => t('Underneath player'),
      'none' => t('None')
    ),
    '#default_value' => $settings['player_settings_saved'] ? $settings['controlbar'] : BD_VIDEO_DEFAULT_CONTROLBAR
  );

  $fieldset['showdownload'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display download button'),
    '#default_value' => $settings['player_settings_saved'] ? $settings['showdownload'] : BD_VIDEO_DEFAULT_SHOWDOWNLOAD,
    '#description' => t("Only works if the source video is still available."),
  );

  $fieldset['autostart'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autostart'),
    '#default_value' => $settings['player_settings_saved'] ? $settings['autostart'] : BD_VIDEO_DEFAULT_AUTOSTART,
    '#description' => 
      t("Start playing the video when the page loads (only applies when the node is being viewed as a page).")
  );

  $fieldset['repeat'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autorepeat'),
    '#default_value' => $settings['player_settings_saved'] ? $settings['repeat'] : BD_VIDEO_DEFAULT_AUTOREPEAT,
    '#description' => t("Automatically repeat the video after it has finished playing."),
  );

  $fieldset['mute'] = array(
    '#type' => 'checkbox',
    '#title' => t('Mute'),
    '#default_value' => $settings['player_settings_saved'] ? $settings['mute'] : BD_VIDEO_DEFAULT_MUTE,
    '#description' => t("Mute audio on startup."),
  );

  $fieldset['volume'] = array(
    '#type' => 'textfield',
    '#title' => t('Volume'),
    '#default_value' => $settings['player_settings_saved'] ? $settings['volume'] : BD_VIDEO_DEFAULT_VOLUME,
    '#size' => 6,
    '#maxlength' => 3, 
    '#attributes' => array('autocomplete' => 'off'),
    '#required' => TRUE,
    '#description' => t('Initial volume level (0 - ') . BD_VIDEO_MAX_VOLUME . ').',
  );

  $fieldset['bufferlength'] = array(
    '#type' => 'textfield',
    '#title' => t('Buffer length'),
    '#default_value' => $settings['player_settings_saved'] ? $settings['bufferlength'] : BD_VIDEO_DEFAULT_BUFFERLENGTH,
    '#size' => 6,
    '#maxlength' => 2, 
    '#attributes' => array('autocomplete' => 'off'),
    '#required' => TRUE,
    '#description' => t('The number of seconds of video that should be buffered before play commences.')
  );
  
  $fieldset['colours'] = array(
    '#type' => 'fieldset',
    '#title' => t('Colours'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );

  $fieldset['colours']['backcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Background colour'),
    '#size' => 6,
    '#maxlength' => 6,
    '#default_value' => $settings['player_settings_saved'] ? $settings['backcolor'] : BD_VIDEO_DEFAULT_BACKCOLOR,
    '#description' => t('Background colour of the controlbar.')
  );

  $fieldset['colours']['frontcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Icon / text colour'),
    '#size' => 6,
    '#maxlength' => 6,
    '#default_value' => $settings['player_settings_saved'] ? $settings['frontcolor'] : BD_VIDEO_DEFAULT_FRONTCOLOR,
    '#description' => t('Colour of all icons and text in the controlbar.')
  );

  $fieldset['colours']['lightcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Hover colour'),
    '#size' => 6,
    '#maxlength' => 6,
    '#default_value' => $settings['player_settings_saved'] ? $settings['lightcolor'] : BD_VIDEO_DEFAULT_LIGHTCOLOR,
    '#description' => t('Colour of an icon or text when you hover over it.')
  );
  
  $fieldset['colours']['screencolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Screen colour'),
    '#size' => 6,
    '#maxlength' => 6,
    '#default_value' => $settings['player_settings_saved'] ? $settings['screencolor'] : BD_VIDEO_DEFAULT_SCREENCOLOR,
    '#description' => t('Background colour of the video.')
  );

  return $fieldset;
}


function _bd_video_player_settings_validate($form_values) {

  if(!is_numeric($form_values['volume'])) {
    form_set_error('volume', t('Volume field must be numeric.'));
  }
  else if($form_values['volume'] < 0) {
    form_set_error('volume', t('Volume field must be greater than or equal to 0.'));
  }
  else if($form_values['volume'] > BD_VIDEO_MAX_VOLUME) {
    form_set_error('volume', t('Volume field must be less than or equal to ') . BD_VIDEO_MAX_VOLUME . '.');
  }
  
  if(!is_numeric($form_values['bufferlength'])) {
    form_set_error('bufferlength', t('Buffer length field must be numeric.'));
  }
  else if($form_values['bufferlength'] < 1) {
    form_set_error('bufferlength', t('Buffer length field must be greater than 0.'));
  }
  else if($form_values['bufferlength'] > BD_VIDEO_MAX_BUFFER_LENGTH) {
    form_set_error('bufferlength', 
      t('Buffer length field must be less than or equal to ') . BD_VIDEO_MAX_BUFFER_LENGTH . '.');
  }
}


function _bd_video_protect_url($url) {

  // Amazon S3 signatures sometimes have an escaped '+' in them.
  // If these '+'s have been unescaped in the http request,
  // it will fail as raw '+'s are interpreted as spaces by s3.amazonaws.com
  
  // It seems that somewhere between the flashvars and the request being made
  // swfobject, jw flv player and/or flash unescape urls a total of three times.
  
  // We therefore need three layers of armour to prevent the '+'s being turned into spaces.
  
  // ...yes, this _is_ ridiculous

  return rawurlencode(rawurlencode(rawurlencode($url)));
}


function _bd_video_player_render($video) {
  static $player_index;
  $player_id = 'blue-droplet-player' . $player_index++;

  drupal_add_js(drupal_get_path('module', 'bd_video') . '/swfobject.js');

  $output = "<div id=\"$player_id\">";
  $output .= "Get the latest <a href=\"http://get.adobe.com/flashplayer/\">Flash Player</a> to see this video.";
  $output .= "</div>\n";

  $width = _bd_video_get_render_setting('player_width', BD_VIDEO_DEFAULT_PLAYER_WIDTH);
  $height = _bd_video_get_render_setting('player_height', BD_VIDEO_DEFAULT_PLAYER_HEIGHT);

  $flashvars[] = 'type=video';
  $flashvars[] = 'file=' . _bd_video_protect_url(storage_api_serve_url($video['flv_file']));

  $image = _bd_video_get_preview_image($video);
  $flashvars[] = 'image=' . _bd_video_protect_url(_bd_video_render_image($video, $image, $width, $height));

  $controlbar = _bd_video_get_render_setting('controlbar', BD_VIDEO_DEFAULT_CONTROLBAR);
  $flashvars[] = 'controlbar=' . $controlbar;
  
  if($controlbar == 'bottom')
    $height += 20;
  
  $options = array(
    'icons' => BD_VIDEO_DEFAULT_ICONS,
    'autostart' => BD_VIDEO_DEFAULT_AUTOSTART,
    'mute' => BD_VIDEO_DEFAULT_MUTE
  );

  foreach($options as $option => $default) {
    $value = _bd_video_get_render_setting($option, $default);
    $flashvars[] = $option . '=' . ($value ? 'true' : 'false');
  }

  if(_bd_video_get_render_setting('repeat', BD_VIDEO_DEFAULT_REPEAT))
    $flashvars[] = 'repeat=single';

  $options = array(
    'volume' => BD_VIDEO_DEFAULT_VOLUME,
    'bufferlength' => BD_VIDEO_DEFAULT_BUFFER_LENGTH,
  );

  foreach($options as $option => $default) {
    $value = _bd_video_get_render_setting($option, $default);
    $flashvars[] = $option . '=' . (int)$value;
  }

  $options = array(
    'backcolor' => BD_VIDEO_DEFAULT_BACKCOLOR,
    'frontcolor' => BD_VIDEO_DEFAULT_FRONTCOLOR,
    'lightcolor' => BD_VIDEO_DEFAULT_LIGHTCOLOR,
    'screencolor' => BD_VIDEO_DEFAULT_SCREENCOLOR
  );

  foreach($options as $option => $default) {
    $value = _bd_video_get_render_setting($option, $default);
    $flashvars[] = $option . '=0x' . $value;
  }

  if(_bd_video_get_render_setting('showdownload', BD_VIDEO_DEFAULT_SHOWDOWNLOAD) && 
    $video['source_file_id'])
  {
    $flashvars[] = 'link=' . _bd_video_protect_url(storage_api_serve_url($video['source_file']));
    $flashvars[] = 'linktarget=_self';
  }

  $params = array(
    'allowfullscreen' => 'true',
//    'allowscriptaccess' => 'always',
//    'wmode' => 'transparent',   // this seems to break the cursor changing
    'flashvars' => implode('&', $flashvars)
  );
  
  switch($video['current_params']['format']) {
  
    case BD_VIDEO_FORMAT_FLV:
      $flash_version = '7.0.14.0';
      break;

    case BD_VIDEO_FORMAT_MP4:
      $flash_version = '9.0.115.0';
      break;
  }
  
  $player_url = base_path() . drupal_get_path('module', 'bd_video') . '/player.swf';


  // <!-- and --> prevent the javascript from being indexed for search
   
  $output .= "<script type=\"text/javascript\"><!--\n";
  $output .= "var s1 = new SWFObject('$player_url','$player_id','$width','$height','$flash_version');\n";
  
  foreach($params as $param => $value)
    $output .= "s1.addParam('$param', '$value');\n";
  
  $output .= "s1.write('$player_id');\n";
  $output .= "--></script>\n";

  return $output;
}

